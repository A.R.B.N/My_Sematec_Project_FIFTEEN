package ir.arbn.www.mysematecprojectfifteen;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class HomeActivity extends AppCompatActivity {
    ImageView myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        myImage = findViewById(R.id.myImage);
        findViewById(R.id.myBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageAction();
                    }
                }, 3000);
            }
        });
    }

    void imageAction() {
        if (myImage.getVisibility() == View.INVISIBLE)
            myImage.setVisibility(View.VISIBLE);
        else myImage.setVisibility(View.INVISIBLE);
    }
}
